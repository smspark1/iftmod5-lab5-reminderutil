The reminder util is a all purpose note takes 

this utility will run the first time and if a .remember file does not exist at the home dir then it will be created and exit
then using the utility requires a first positional parameter of note or remindme
If note (lowercase important) 
	then it will open a prompt and you can type what ever you want and use ctrl D to exit. 
	If note is present as the first parameter and then additional parameters are passed after it then it will append the additional parameters to the remember file (for example script.sh note this is my note = this is my note will be added to the remember file 

IF remindme is passed as 1st positional (lowercase important) 
	If remind me is passed without any additional args then the whole remember file will be presented control c to get out
	If remind me is presented and then search is the second positional then you will get a prompt to pass something via grep to search the file. 

Everything else will essentially exit the script with a text line to view this very file. 
Help and credit to Wicked Cool Shell Scripts by Dave Taylor and Brandon Perry 
